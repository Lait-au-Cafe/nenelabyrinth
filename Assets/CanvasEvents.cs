﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasEvents : MonoBehaviour {
    public Button btnRestart;
    public Button btnQuit;

    // Use this for initialization
	void Start () {
        btnRestart.onClick.AddListener(() =>
        {
            SceneManager.LoadScene(0);
        });
        btnQuit.onClick.AddListener(() =>
        {
            Application.Quit();
        });
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
