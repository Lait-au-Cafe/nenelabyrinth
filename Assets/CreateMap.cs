﻿using UnityEngine;
using System.Collections;

public class CreateMap : MonoBehaviour {
    public uint size = 51;
    public float unit = 0.08f;
    public PhysicMaterial wallMaterial;

    // Use this for initialization
    void Start() {
        // 火が吹いたりとか、風が吹いたりとか
        // 起動直後にランダムに迷路の自動生成とか
        // いろいろやろうとしてたら訳わからなくなってきたので
        // ここで処理を止める
        //Time.timeScale = 0;

        GameObject floor = gameObject;
        floor.transform.localScale = new Vector3(size * unit, 0.25f, size * unit);
        floor.transform.localPosition = new Vector3((size - 1) * unit * 0.5f, -0.25f / 2, (size - 1) * unit * 0.5f);

        bool[] maze;
        GenerateMaze(size, out maze);

        GameObject go;
        Rigidbody rb;
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                if (!maze[i + j * size])
                {
                    go = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    go.transform.localScale = new Vector3(unit, unit, unit);
                    go.transform.position = new Vector3(i * unit, unit / 2, j * unit);

                    // 物理演算関係
                    rb = go.AddComponent<Rigidbody>();
                    rb.useGravity = false;
                    rb.isKinematic = true;
                    rb.interpolation = RigidbodyInterpolation.Interpolate;
                    rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
                    go.transform.GetComponent<Collider>().material = wallMaterial;

                    // 親を登録
                    go.transform.SetParent(transform);
                }
            }
        }

        Transform player = GameObject.FindGameObjectWithTag("Player").transform;
        player.position = new Vector3((size - 2) * unit, 0.1f, (size - 1) * unit);
        player.SetParent(transform);

        Transform goal = GameObject.FindGameObjectWithTag("Goal").transform;
        goal.position = new Vector3( unit, unit / 3, -unit);
        goal.SetParent(transform);

        transform.localPosition = Vector3.zero;
        player.parent = null;
    }

    enum Directions { Up, Down, Left, Right };

    void GenerateMaze(uint size, out bool[] result)
    {
        uint width = size + 2; // + pivot
        bool[] map = new bool[width * width];
        int i, j;

        // 初期化
        for (i = 0; i < width; i++)
        {
            for (j = 0; j < width; j++)
            {
                map[i + j * width] = i == 0 || i == width - 1 | j == 0 | j == width - 1;
            }
        }

        uint start = (width - 3) + 2 * width;
        uint goal = 2 + (width - 3) * width;

        // スタートの設定
        uint cur = start;
        map[cur] = true;

        // 迷路を生成
        Directions[] dirs;
        uint prev, pos;
        uint tryCount = 0; // 行きどまった時の再試行回数
        while(true)
        {
            dirs = shuffle();
            prev = cur;

            foreach (var d in dirs)
            {
                switch (d)
                {
                    case Directions.Down:
                        pos = cur + 2 * width;
                        if (!map[pos])
                        {
                            map[pos] = map[cur + width] = true;
                            cur = pos;
                        }
                        break;

                    case Directions.Left:
                        pos = cur - 2;
                        if (!map[pos])
                        {
                            map[pos] = map[cur - 1] = true;
                            cur = pos;
                        }
                        break;

                    case Directions.Right:
                        pos = cur + 2;
                        if (!map[pos])
                        {
                            map[pos] = map[cur + 1] = true;
                            cur = pos;
                        }
                        break;

                    case Directions.Up:
                        pos = cur - 2 * width;
                        if (!map[pos])
                        {
                            map[pos] = map[cur - width] = true;
                            cur = pos;
                        }
                        break;

                    default:
                        break;
                }       

                if(prev != cur)
                {
                    break;
                }
            }

            // 行き止まり
            if(prev == cur)
            {
                if (tryCount > size * size * size && map[goal])
                {
                    break;
                }
                else
                {
                    int x, y;
                    do
                    {
                        int min = 1;
                        int max = (int)(width - 3) / 2;
                        x = 2 * Random.Range(min, max);
                        y = 2 * Random.Range(min, max);
                        pos = (uint)(x + y * width);
                        //Debug.Log(x + ", " + y);
                    } while (!map[pos]);
                    cur = pos;
                    tryCount++;
                }
            }
        }

        // スタートとゴールには穴をあける
        map[start - width] = map[goal + width] = true;
        

        // 結果を作成
        result = new bool[size * size];
        for (i = 1; i < width - 1; i++)
        {
            for (j = 1; j < width - 1; j++)
            {
                //result[(i - 1) + (j - 1) * size] = map[i + j * width];
                result[(i - 1) + (size - 1 - (j - 1)) * size] = map[i + j * width];
            }
        }
    }

    Directions[] shuffle()
    {
        Directions[] dirs = (Directions[])System.Enum.GetValues(typeof(Directions));
        int length = dirs.Length;

        for(int i = 0; i < length; i++)
        {
            var tmp = dirs[i];
            int randomIndex = Random.Range(0, length);
            dirs[i] = dirs[randomIndex];
            dirs[randomIndex] = tmp;
        }

        return dirs;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
