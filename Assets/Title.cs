﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {
    public Texture2D tex;
    public Texture2D tex1;
    public Texture2D tex2;

    Texture2D fg;
    Texture2D bg;
    RawImage rawImage;

    bool isFg;
    bool selected;

    // Use this for initialization
    IEnumerator Start () {
        //tagでUIを表示するCameraを判断
        Camera cam = Camera.main;

        Canvas canvas = transform.parent.GetComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceCamera;
        canvas.planeDistance = cam.farClipPlane - 0.01f;
        canvas.worldCamera = cam;

        var width = tex1.width;
        var height = tex1.height;

        rawImage = GetComponent<RawImage>();
        rawImage.texture = tex1;    // テクスチャをセット
        RectTransform recTrans = GetComponent<RectTransform>();
        float scale = ((width / height) > cam.aspect) ? ((float)cam.pixelHeight / height) : ((float)cam.pixelWidth / width);
        recTrans.sizeDelta = new Vector2(scale * width, scale * height);

        fg = tex1;
        bg = tex;

        //StartCoroutine(UpdateX());

        isFg = false;
        selected = false;

        while (!selected)
        {
            if (isFg)
            {
                rawImage.texture = bg;
            }
            else
            {
                rawImage.texture = fg;
            }

            isFg = !isFg;

            yield return new WaitForSecondsRealtime(0.5f);
        }

        for(int i = 0; i < 8; i++)
        {
            if (isFg)
            {
                rawImage.texture = bg;
            }
            else
            {
                rawImage.texture = fg;
            }

            isFg = !isFg;

            yield return new WaitForSecondsRealtime(0.1f);
        }

        if(fg == tex1)
        {
            SceneManager.LoadScene(1);
        }
        else
        {
            SceneManager.LoadScene(2);
        }
    }

    void Update()
    {
        if (selected)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            selected = true;
            isFg = false;
        }
        if (Input.GetKeyDown("w"))
        {
            fg = tex1;
            isFg = false;
        }
        if (Input.GetKeyDown("s"))
        {
            fg = tex2;
            isFg = false;
        }
    }
}
