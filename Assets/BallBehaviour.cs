﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallBehaviour : MonoBehaviour {
    private Rigidbody rb;
    public Canvas canvas;
    public Transform floor;
    CanvasGroup cg;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        if(canvas != null)
        {
            cg = canvas.GetComponent<CanvasGroup>();
            cg.interactable = false;
            cg.alpha = 0;
        }
        StartCoroutine(Sway());
	}
	
	// Update is called once per frame
	void FixedUpdate () { 
        if(Mathf.Abs(transform.localPosition.y) > 100)
        {
            //Destroy(gameObject);
            SceneManager.LoadScene(0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        switch (other.tag)
        {
            case "DeathZone":
                // Jet to the Future
                rb.velocity = new Vector3(0, 3, 0);
                rb.useGravity = false;
                break;
            case "HoppingJump":
                StartCoroutine(Hopping());
                break;
            case "GoalZone":
                if(cg != null)
                {
                    cg.interactable = true;
                    cg.alpha = 1.0f;
                }
                break;
            default:
                break;
        }
    }

    IEnumerator Hopping()
    {
        if(floor == null)
        {
            yield break;
        }

        Vector3 prev_vel = rb.velocity;

        //float amp = 0.1f;
        //Vector3 vel = new Vector3(Mathf.Sign(rb.velocity.x) * 2f, 4f, 0) * amp;
        float unit = rb.velocity.x;
        float ratio = 8f;
        Vector3 vel = new Vector3(unit, ratio * unit, 0);
        rb.useGravity = false;

        rb.velocity = vel;
        yield return new WaitForSeconds(0.1f);

        rb.velocity = Vector3.zero;
        yield return new WaitForSeconds(0.2f);

        rb.velocity = new Vector3(unit, -ratio * unit, 0);
        yield return new WaitForSeconds(0.1f);

        rb.velocity = prev_vel;
        rb.useGravity = true;
    }

    IEnumerator Sway()
    {
        while (true)
        {
            int cnt = Random.Range(1, 8);
            float waitTime = Random.Range(0.2f, 0.4f);
            for (int i = 0; i < cnt; i++)
            {
                transform.position = transform.position - new Vector3(0, -0.01f, 0);
                yield return new WaitForSeconds(0.1f);
            }
            yield return new WaitForSeconds(waitTime);
        }
    }
}
