﻿using UnityEngine;
using System.Collections;

public class ControllerRelative : MonoBehaviour
{
    public Vector3 tilt = new Vector3(0, 0, 0);

    public float maxAngle = 15f;
    public float deltaAngle = 15f;

    Vector3 defaultGravity;

    Transform cam;
    SE3 orig_cam_post;

    class SE3
    {
        public readonly Vector3 pos;
        public readonly Quaternion rot;

        public SE3(Quaternion q, Vector3 x)
        {
            rot = q;
            pos = x;
        }

        public static SE3 operator* (SE3 left, SE3 right)
        {
            return new SE3(left.rot * right.rot, left.rot * right.pos + left.pos);
        }

        public SE3 inverse
        {
            get
            {
                Quaternion inv = Quaternion.Inverse(rot);
                return new SE3(inv, inv * -pos);
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        //defaultGravity = Physics.gravity;
        defaultGravity = new Vector3(0, -9.81f, 0);

        cam = Camera.main.transform;
        cam.localScale = new Vector3(1, 1, 1);
        orig_cam_post = new SE3(cam.rotation, cam.position);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        //=====================================================================
        // Rotate
        //=====================================================================

        var angle = new Vector3(0, 0, 0);

        if (Input.GetKey("w"))
        {
            angle.x++;
        }
        if (Input.GetKey("a"))
        {
            angle.z++;
        }
        if (Input.GetKey("s"))
        {
            angle.x--;
        }
        if (Input.GetKey("d"))
        {
            angle.z--;
        }

        tilt.x = Mathf.Clamp(tilt.x + deltaAngle * Time.deltaTime * angle.x, -maxAngle, maxAngle);
        tilt.z = Mathf.Clamp(tilt.z + deltaAngle * Time.deltaTime * angle.z, -maxAngle, maxAngle);

        // 姿勢の再計算
        var v = tilt.normalized;
        var rad = Mathf.Deg2Rad * tilt.magnitude / 2;
        v *= Mathf.Sin(rad);
        Quaternion q = new Quaternion(v.x, v.y, v.z, Mathf.Cos(rad));

        // 重力方向の変更
        Physics.gravity = Quaternion.Inverse(q) * defaultGravity;

        // カメラ姿勢の変更
        SE3 trans = new SE3(q, Vector3.zero);
        SE3 new_cam_post = trans.inverse * orig_cam_post;
        cam.position = new_cam_post.pos;
        cam.rotation = new_cam_post.rot;

    }
}
