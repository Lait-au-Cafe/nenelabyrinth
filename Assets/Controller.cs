﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {
    public Vector3 tilt = new Vector3(0, 0, 0);

    public float maxAngle = 15f;
    public float deltaAngle = 15f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {

        //=====================================================================
        // Rotate
        //=====================================================================

        var angle = new Vector3(0, 0, 0);

        if (Input.GetKey("w"))
        {
            angle.x++;
        }
        if (Input.GetKey("a"))
        {
            angle.z++;
        }
        if (Input.GetKey("s"))
        {
            angle.x--;
        }
        if (Input.GetKey("d"))
        {
            angle.z--;
        }
        
        //var prev = tilt;
        tilt.x = Mathf.Clamp(tilt.x + deltaAngle * Time.deltaTime * angle.x, -maxAngle, maxAngle);
        tilt.z = Mathf.Clamp(tilt.z + deltaAngle * Time.deltaTime * angle.z, -maxAngle, maxAngle);

        /*
        var v = prev.normalized;
        var rad = Mathf.Deg2Rad * prev.magnitude / 2;
        v *= Mathf.Sin(rad);
        Quaternion q1 = new Quaternion(v.x, v.y, v.z, Mathf.Cos(rad));
        */

        var v = tilt.normalized;
        var rad = Mathf.Deg2Rad * tilt.magnitude / 2;
        v *= Mathf.Sin(rad);
        Quaternion q2 = new Quaternion(v.x, v.y, v.z, Mathf.Cos(rad));

        transform.rotation = q2;
    }
}
